import { Injectable } from '@angular/core';

@Injectable()
export class WeatherService {

  constructor() { }

  getApixuApiKey = () => {
    const apiKey = '3601befdb5a34969b18193058181701'
    return apiKey
  }

  getCurrentWeatherByCity = (city: string) => {
      const getKey = this.getApixuApiKey()
      const cityDetails = `https://api.apixu.com/v1/forecast.json?key=${getKey}&q=${city}&days=7`
      return cityDetails
  }

}
