import { Component, OnInit } from '@angular/core';
import { WeatherService } from './../weather.service'
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {

  forecast:object
  current:object
  location:object
  today:string
  data:any
  error:boolean

  constructor(private http: HttpClient, private  weatherService: WeatherService) {
    const date  = new Date();
    this.today = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate()
    this.error = false
  }

  ngOnInit() {
    this.http.get(this.weatherService.getCurrentWeatherByCity('Szeged')).subscribe(data => {
      this.forecast = data['forecast']['forecastday'];
      this.current = data['current']
      this.location = data['location']
    });
  }

  getLocationByCity(city:string){
    this.http.get(this.weatherService.getCurrentWeatherByCity(city)).subscribe(data => {
        this.error = false
        this.forecast = data['forecast']['forecastday'];
        this.current = data['current']
        this.location = data['location']
    }, error => {
      this.error = true
    });
  }

}
